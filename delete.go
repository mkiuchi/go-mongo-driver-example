package main

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	// MongoDBの接続設定
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	c, _ := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://epgrec2.mkhome:27017"))
	defer c.Disconnect(ctx)

	// MongoDBのCollectionを取得
	col := c.Database("epgrec").Collection("autorec")

	// 検索条件となるprimitive.ObjectID型の変数を指定
	objectID, _ := primitive.ObjectIDFromHex("5d1924916a81c3556cf3479b")

	// 検索を実行し、結果のドキュメントを削除
	_, err := col.DeleteOne(context.Background(), bson.M{"_id": objectID})
	if err != nil {
		fmt.Println("delete failed:", err)
	} else {
		fmt.Println("delete success")
	}
}

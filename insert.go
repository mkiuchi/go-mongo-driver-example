package main

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	// MongoDBの接続設定
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	c, _ := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://epgrec2.mkhome:27017"))
	defer c.Disconnect(ctx)

	// 挿入するデータの構造を定義
	// 21, 22行目の末尾の文字はType aliasで、
	// MongoDB内のフィールド名として解釈される
	type dataType struct {
		Rid     string `bson:"rid"`
		Keyword string `bson:"keyword"`
	}

	// 挿入するデータを作成
	data := dataType{
		Rid:     "俺のID",
		Keyword: "俺のキーワード",
	}

	// MongoDBのCollectionを取得
	col := c.Database("epgrec").Collection("autorec")

	// データを挿入
	col.InsertOne(context.Background(), data)

}

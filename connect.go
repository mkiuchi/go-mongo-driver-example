package main

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func main() {
	// コンテキストの作成
	//   - バックグラウンドで接続する。タイムアウトは10秒
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	// 関数を抜けたらクローズするようにする
	defer cancel()
	// 指定したURIに接続する
	c, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://epgrec2.mkhome:27017"))
	defer c.Disconnect(ctx)
	// DBにPingする
	err = c.Ping(ctx, readpref.Primary())
	if err != nil {
		fmt.Println("connection error:", err)
	} else {
		fmt.Println("connection success:")
	}
}

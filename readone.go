package main

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	// MongoDBの接続設定
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	c, _ := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://epgrec2.mkhome:27017"))
	defer c.Disconnect(ctx)

	// 結果を格納する変数の構造定義
	type resultType struct {
		Rid     string
		Keyword string
	}

	// 結果を格納する変数を宣言
	var result resultType

	// MongoDBのCollectionを取得
	col := c.Database("epgrec").Collection("autorec")

	// 検索条件となるprimitive.ObjectID型の変数を指定
	objectID, _ := primitive.ObjectIDFromHex("5cffa613c74a91322fc7cbb2")

	// 検索を実行し、結果を変数 result に格納
	err := col.FindOne(context.Background(), bson.M{"_id": objectID}).Decode(&result)
	_ = err
	fmt.Println(result)
}

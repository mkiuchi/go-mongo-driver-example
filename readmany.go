package main

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	// MongoDBの接続設定
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	c, _ := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://epgrec2.mkhome:27017"))
	defer c.Disconnect(ctx)

	// 結果を格納する変数の構造定義
	type resultType struct {
		Start int
		Title string
	}

	// MongoDBのCollectionを取得
	col := c.Database("epgrec").Collection("rec")

	// 検索条件となるint型の変数を指定
	location, _ := time.LoadLocation("Asia/Tokyo")
	start := time.Date(2019, 6, 30, 0, 0, 0, 0, location).Unix() * 1000
	end := time.Date(2019, 6, 30, 23, 59, 59, 99, location).Unix() * 1000
	fmt.Println(start, end)

	// 検索を実行
	cur, err := col.Find(context.Background(), bson.M{
		"start": bson.M{
			"$gte": start,
			"$lte": end,
		}})
	_ = err

	// 結果のカーソルをforで回して順番に結果を取得
	for cur.Next(context.Background()) {
		var ret resultType
		cur.Decode(&ret)
		fmt.Println(ret)
	}
}
